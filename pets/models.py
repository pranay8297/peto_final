from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from pets.search import PetIndex


class Pet(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="publisher",
        on_delete=models.CASCADE)
    pet_name = models.CharField(max_length=30)
    breed = models.CharField(max_length=100)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    vaccinated = models.BooleanField(default=True)
    sterlized = models.BooleanField(default=True)
    age = models.IntegerField()
    general_behaviour = models.CharField(max_length=100)
    description = models.CharField(max_length=400)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, null=True)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, null=True)
    address = models.CharField(max_length=1000)
    is_adopted = models.BooleanField(default=False)

    def indexing(self):
        print(" inside model indexing")
        peto = PetIndex(
            meta={'id': self.id},
            pet_name=self.pet_name,
            breed=self.breed,
            id=self.id
        )
        peto.save()
        return peto.to_dict(include_meta=True)


class Image(models.Model):
    pet_id = models.ForeignKey(
        Pet, on_delete=models.CASCADE, related_name='image')
    image = models.ImageField(upload_to='images/')
