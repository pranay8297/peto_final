from django import forms
from django.forms import ModelForm

# from markdownx.fields import MarkdownxFormField

from pets.models import Pet, Image

class PetForm(ModelForm):

    class Meta:
        model = Pet
        fields = ('pet_name',
                  'breed',
                  'gender',
                  'vaccinated',
                  'age', 
                  'sterlized',
                  'gender',
                  'general_behaviour',
                  'description',
                  'address')

class ImageForm(ModelForm):
    
    class Meta:
        model = Image
        fields = ('image',)

