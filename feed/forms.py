from django import forms
from django.forms import ModelForm

from feed.models import Post, Image, Comment


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = (
            'title',
            'content',
        )


class ImageForm(ModelForm):
    class Meta:
        model = Image
        fields = (
            'image',
        )


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = (
            'comment_content',
        )
