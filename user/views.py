from django.shortcuts import render
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
# Create your views here.
from django.contrib.auth import login, authenticate, logout
from django.shortcuts import redirect
from .forms import ProfileForm, LoginForm
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from .models import Profile
import requests
import json
import random
from rest_framework.views import APIView

# dummy lat and long values for testing
list_of_locations = [[16.166700, 74.833298], [26.850000, 80.949997],
                     [28.610001, 77.230003], [19.076090, 72.877426],
                     [14.167040, 75.040298], [26.540457, 88.719391], ]


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        profile_form = ProfileForm(request.POST)
        if form.is_valid() and profile_form.is_valid():
            new_form = form.save()
            new_profile = profile_form.save(commit=False)
            new_profile.user = new_form
            new_profile.save()
            # username = form.cleaned_data.get('username')
            # raw_password = form.cleaned_data.get('password')
            # user = authenticate(username=username, password=raw_password)
            # login(request, user)

            return redirect("user:login")
        else:
            messages.error(request, 'Enter proper credential')
            return redirect('user:signup')
    else:
        form = UserCreationForm()
        profile_form = ProfileForm()
        return render(request, 'user/signup.html', {'form': form,
                                                    'profile_form': profile_form})

# get client's ip

# get user location


def get_client_address(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    location = requests.get('http://ip-api.com/json/'+ip)
    json_data = json.loads(location.text)
    return json_data


def loginView(request):
    if request.method == 'POST':
        
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(
            request, username=username, password=password)

        if user is not None:
            #location = get_client_address(request)

            id = user.id
            login(request, user)
            person = Profile.objects.get(id=id)
            # person.longitude = random.choice(list_of_locations)[1]
            # person.latitude = random.choice(list_of_locations)[0]
            person.save()
            return redirect("user:home")
        else:
            messages.error(request, 'username or password not correct')
            return redirect('user:login')

    else:
        login_form = LoginForm()
        return render(request, 'user/login.html', {'login_form': login_form})


def homeView(request):
    return redirect("pets:pet-list")


def logout_view(request):
    logout(request)
    return redirect("user:login")


class ProfileView(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'user/profile.html'

    def get(self, request, format=None):
        id = request.user.id

        person = Profile.objects.get(id=id)
        person.name = request.user.username
        return Response({'person': person})
